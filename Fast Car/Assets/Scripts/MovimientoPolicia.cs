﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovimientoPolicia : MonoBehaviour
{
    public float velocidad = 4f;
    public bool siguiendo = true;
    public float rangoDeteccion = 10f;
    public float rangoAtaque = 1.5f;
    public AudioClip sirena;
    Vida personaje;
    NavMeshAgent agent;
    public float cooldown = 1.5f;
    public float cooldownTimer;


    public Estados estadoActual;

   

    public enum Estados
    {
        Estado1,
        Estado2,
        
    }


    void Start()
    {
        StartCoroutine(InicioMaquinaEstado());
        agent = GetComponent<NavMeshAgent>();
        estadoActual = Estados.Estado1;
        personaje = FindObjectOfType<Vida>();
        agent.speed = velocidad;
        //SonidoSierna();
    }

    void FixedUpdate()
    {


        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopAllCoroutines();
        }

        float distanciaAlPersonaje = Vector3.Distance(transform.position, personaje.transform.position);



        if (cooldownTimer == 0)
        {
        

            if (siguiendo == true)//Encontro al personaje
            {
                if (distanciaAlPersonaje > rangoAtaque)//Si el enemigo esta lejos del pj entonces solo lo sigo
                {

                    
                    agent.destination = personaje.transform.position;//NavMeshAgent.Warp(personaje.transform.position);//
                    transform.LookAt(personaje.transform.position);
                }
             

            }


        }
        else
        {
            siguiendo = false;
        }
    }

    public void CooldownPararse()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;

        }
        if (cooldownTimer < 0)
        {
            cooldownTimer = 0;
        }
    }

    void OnDrawGizmos()
    {

        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, rangoDeteccion);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangoAtaque);
    }

    public void SonidoSierna()
    {
        AudioManager.Instance.playSonido(sirena,0.3f);
        
    }




    IEnumerator InicioMaquinaEstado()
    {

        while (true)
        {

            switch (estadoActual)
            {

                case Estados.Estado1:

                    yield return StartCoroutine(CREstado1());

                    break;

                case Estados.Estado2:

                    yield return StartCoroutine(CREstado2());

                    break;

                
                default:

                    break;

            }
        }
    }

    IEnumerator CREstado1()
    {

        

        

        
       Light LuzAzul = GetComponentInChildren<Light>();
        if (LuzAzul.tag == "LuzAzul") {
            LuzAzul.intensity = 10;
        }
        if (LuzAzul.tag == "LuzRoja")
        {
            LuzAzul.intensity = 0;
        }
        yield return new WaitForSeconds(1f);
        estadoActual = Estados.Estado2;

    }

    IEnumerator CREstado2()
    {


        Light LuzRoja = GetComponentInChildren<Light>();
        if (LuzRoja.tag == "LuzRoja")
        {
            LuzRoja.intensity = 10;
        }
        if (LuzRoja.tag == "LuzAzul")
        {
            LuzRoja.intensity = 0;
        }
        yield return new WaitForSeconds(1f);

        estadoActual = Estados.Estado1;
    }
}
