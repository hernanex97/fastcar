﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTransitoAvenida : MonoBehaviour
{
	public float distanciaCalle;
	public float cantidadMetrosSeparacion;
	public float cantidadAutosEnCarril;

    float posicionEnZParaInstanciar;

    int contadorAutos;

    public GameObject[] poolSpawners;
    GameObject[] poolAutos;
    LevelManager levelManagerScript;

    Vector3 posicionAuto;

    public int anguloEnY; 

    void Start()
    {
        posicionEnZParaInstanciar = poolSpawners[0].transform.position.z;
        levelManagerScript = FindObjectOfType<LevelManager>();
        poolAutos = levelManagerScript.poolAutos;
        //poolSpawners = levelManagerScript.poolSpawners;

        cantidadAutosEnCarril = distanciaCalle / cantidadMetrosSeparacion;
        
        while (contadorAutos < cantidadAutosEnCarril)
        {
            InvocarAuto();
            levelManagerScript.cantidadAutos++;
            contadorAutos++;
        }
    }

    public void InvocarAuto()
    {
        posicionEnZParaInstanciar = posicionEnZParaInstanciar + cantidadMetrosSeparacion; 
        posicionAuto = new Vector3(poolSpawners[Random.Range(0, poolSpawners.Length)].transform.position.x , 0, posicionEnZParaInstanciar);

        Quaternion anguloAuto = Quaternion.Euler(transform.rotation.x, anguloEnY, transform.rotation.z);
        GameObject.Instantiate(poolAutos[Random.Range(0, poolSpawners.Length)], posicionAuto, anguloAuto);
    }

        
}
